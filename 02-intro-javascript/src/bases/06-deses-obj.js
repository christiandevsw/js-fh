
//Desestruturacion o asignacion Desestruturante

const person={
    name: 'Tony',
    age: 21,
    key: 'Ironman',
    range:'Soldado',
};

const {name:name2}=person;
console.log(name2);

const {name,age,key}=person;
console.log(name,age,key);


// const printPerson=(user)=>{
//     const{age,key,name}=user;
//     console.log(age,key,name);
// };
const useContext=({key,name,age,range='Capitán'})=>{ //desestructuracion en argumento
    // segun autor: la deses tambien nos permite agregar valores por ejdefectos ej range sospecho que ello
    //se debe mas por el tema de parametros x defecto 
    
    return {
        keyWord:key,
        age,
        latlng:{
            lat:14.1232,
            lng:-12.3232
        }
    };
};
const {keyWord,age:years,latlng:{lat,lng}} = useContext(person); // {...latlng:{lat,lng}}, desest anidada poco comun
//si {...,latlng}=... , si fuese asi, hubiera:  
// const {lat,ln}=latlng;

console.log(keyWord,years);
console.log(lat,lng);

