

const person={
    name:'Tony',
    lastName:'Stark',
    age:45,
    address:{
        city:'New York',
        zip:55321321,
        lat:14.3232,
        lng:34.9233321
    }
};

// console.table({
//     person
// });
console.log(person);

//Copiamos la referencia no el valor en si
const person2=person; //jamas haceer este tipo de asignacion porque afecta doble via
const person3={...person};  //asi se asigna
person.name='Peteri';
console.log(person);
console.log(person2);