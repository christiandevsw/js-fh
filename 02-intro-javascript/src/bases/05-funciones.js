
// Revienta por cambio de referencia no controlado
// function greeting(name){
//     return `Hola, ${name}`;
// }

const greeting2=function(name){
    return `Hola, ${name}`;
};

    // greeting=30;
    


// un solo return entonces se convierte a 
// const greeting3=(name)=>{
//     return `Hola, ${name}`;     
// };

const greeting3=(name)=>(`Hola, ${name}`);   //los () no altera pero no es necesario en este caso dado q esta retonando un primitivo
    
   

// console.log(greeting('Goku'));
console.log(greeting3);
console.log(greeting3('Vegeta'));

//En js podemos retornar objetos, va mas alla de los lenguajes tradicionales
const getUser=()=>{
    return {
        uid: 'ABC123',
        username: 'El_papi1502'
        };
};

const getUser2=()=>
        ({
        uid: 'ABC123',
        username: 'El_papi1502'
        });
//no pasa nada que no estamos asignando objetos literales
const user=getUser();
console.log(user);
console.log(getUser2());

const message=()=>{
    console.log('Hola mundo');
};

const message2=()=>console.log('Hola mundo2');

message();
message2();


//TAREA
function getActiveUser(name){
    return {
        uid: 'ABC567',
        username: name
    };
}
const activeUser = getActiveUser('Fernando');
console.log(activeUser);

const activeUser2 = (name)=>
    ({
        uid: 'ABC567',
        username: name
    });
console.log(activeUser2('Fernando'));